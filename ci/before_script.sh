#!/usr/bin/env bash

set -e
set -x
mkdir -p /home/gitlab-runner/.cache/unity3d
mkdir -p /home/gitlab-runner/.local/share/unity3d/Unity/
set +x

UPPERCASE_BUILD_TARGET=${BUILD_TARGET^^};

LICENSE="UNITY_LICENSE_"$UPPERCASE_BUILD_TARGET

if [ -z "${!LICENSE}" ]
then
    echo "$LICENSE env var not found, using default UNITY_LICENSE env var"
    LICENSE=UNITY_LICENSE
#	echo $LICENSE
else
    echo "Using $LICENSE env var"
fi

echo "Writing $LICENSE to license file /root/.local/share/unity3d/Unity/Unity_lic.ulf"
echo "${!LICENSE}" | tr -d '\r' > /home/gitlab-runner/.local/share/unity3d/Unity/Unity_lic.ulf
#cat /home/gitlab-runner/.local/share/unity3d/Unity/Unity_lic.ulf

set -x
