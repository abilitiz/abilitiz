# Contribution Guidelines

### Instructions d'installation

<!-- A remplir -->

### Les Issues

<!-- A remplir -->

### Labels

`CI/CD` pour ce qui concerne les pipelines, au deploiement par gitlab-runner, et tests auto

`GitLab` pour ce qui concerne la gestion du projet et du code dans GitLab

`R&D` pour les recherches sur les apports possibles de nouveaux outils et technologies

`amélioration` pour les modifications qui peuvent améliorer le projet (performance, jouabilité, gestion de projet, sécurité, code ...)

`data` pour tout ce qui est rapport avec les données générées et utilisées (bases de données, sql, nosql, MCD, MLD)

`design` pour ce qui concerne la philosophie même du jeu, son gameplay, ses objectifs, bref le game-design

`dev` pour tout ce qui est à gérer par l'équipe DEV

`ops` pour tout ce qui est à gérer par l'équipe OPS

`docker` pour ce qui concerne les conteneurs, les images, le fichier docker-compose

`documentation` pour le fichier readme, les templates, le wiki, les messages de commits

`unity` pour les fichiers générés par l'éditeur Unity (assets, scripts, build, projet, sprites)

`bug` pour le signalement de bugs

`suggestion` pour les demandes de changement

### Changement du code

1. **Commit often**

    You do not need to have a lot of changes to create a commit. On the contrary, you should try to split your changes into small logical steps and commit each of them. They should be consistent, self-contained (work independently of later commits), and pass the same test which previously did (or more) to ensure you do not introduce any regression. On the other hand, do not push it to the extreme: if your changes are tightly related and stay clear as a whole, there is no need to create multiple commits, even if they are applied to different files, methods, etc.
    >TIP: `git gui` will allow you to stage changes by lines or by hunk instead of the whole file. It is extremely useful when your recent changes could be divided in multiple commits.

2. **Style only rhymes with style**

    Style changes can introduce a big number of diffs in your whole project (take a variable name change, tabulation in a whole file...). If commited as part of a commit for enhancement or bug fixes, it becomes very difficult for a reviewer to track down the changes specific to your work, and the ones specific to style changes. For that reason, prefer grouping style changes in a separate commit.

3. **Push often**

    A good reason to push often is to have your work somewhere else than just on your computer in the event of something were to happen to local computer. Another reason is to facilitate code sharing: even if your branch is not at maturity, creating a pull request as [WIP] will allow you to discuss your progress, ask questions, and get feedbacks during the development process and not just at the end.

4. **Merge often...**

    The whole idea behind continuous integration (or CI) is to integrate your own code with the main repository as often as you can. If you make small changes and put them into single commits you may integrate them often (and probably should). Doing so minimizes merge conflicts with your team members. You can read more about CI in a Git context in Martin Fowler’s excellent [FeatureBranch article].

5. **... but never the other way around!**

    If your branch and the master branch diverge and are in conflicts, never resolve those by merging master into your branch. Instead, rebase your branch against master. This will allow you to adress the conflicts where they were introduced instead of commiting a patch complicated to review, and will keep the commit history clean.

Some of the content above was directly or partially taken from Crealytics blog : [5 reasons for keeping your git commits small](https://crealytics.com/blog/2010/07/09/5-reasons-keeping-git-commits-small/).

### Messages de commit

Proper commit messages are important as they allow to speed up the review process. They are also crucial for development down the road to be able to come back to that commit and understand the logic behind the changes.

1. Describe thoroughly

    Assume the reviewers do not know your motivation behind your work: every change you make has a reason you need to explicitly write down in the commit description, even the little things (ex: why changing a default value). This will accelerate the review process by allowing reviewers to rely on your work description instead of having to contact you for answers.

2. Document accordingly

    A great addition is to offer links to additional information like your source (reference to the algorithm you are using, online discussion supporting your changes...), hash of the commit that introduced the regression being fixed, etc.

3. Be specific

    When making changes to multiple classes or refering to other classes than the one being modified, be specific about class functions and member variables by properly specifying the namespace: `Class:function()` or `Class::m_member`.

4. List changes

    If your commit is composed of multiple changes in order to stay self-contained, list your changes instead of appending them in long paragraphs for clarity.

5. Use a clear and short commit title

    Describe the topic in a couple words while being as specific as possible. "Improve rendering" or "Fix controllers" would be considered too broad.

6. Use the appropriate title prefix to help quickly understand broad context
  * **ENH:** For enhancement, new features, etc.
  * **PERF:** For improved compile and runtime performance and algorithmic optimizations.
  * **BUG:** For fixing bugs of all kinds.
  * **COMP:** For compilation error fix.
  * **TEST:** For new or improved testing.
  * **DOC:** For documentation improvements.
  * **STYLE:** For changes which do not impact the code output but rather the way the code is presented or organized.


### Créer une Merge Requests

All work should start with an [issue on GitLab](https://gitlab.com/NickBusey/HomelabOS/-/issues/new). After creating a detailed issue, it's usually best to wait a day or two before beginning work, to gather feedback from the community.

A good MR is small and changes as few lines of code as possible to resolve the issue. Multiple smaller separate MRs are ofter better than one big MR that touches multiple areas of the code. They're easier to review, test, and merge.

A large MR with several unrelated fixes in it can be held up from being merged because of one particular thing that only affected one piece of the MR. Had they been separate MRs, only the MR that has a problem or needs a change will be held up.
