# Projet Abilitiz

La démo du jeu (dev build) est dispo [ici](https://unity.apollonian.fr/) !

## Design

Le projet est conçu pour être déployé par Docker Compose.

Chaque brique a son propre conteneur, dans son propre répertoire.

Les conteneurs déployés sont :
- Frontal Web
- ???

## Développement

Le projet est organisé en plusieurs branches :
- Master : branche de production (https://abilitiz.apollonian.fr). Mises à jours par merge request à partir de la branche test.
- Test : branche de test d'application (https://test.apollonian.fr et https://unity.apollonian.fr). Mises à jours par merge depuis les autres branches. Les autres branches doivent être créées à partir de la branche test.
- Doc : mise à jour du README
- docker-x : dédiés à chaque conteneur
- unity-x : dédis au projet Unity

Le développement d'une nouvelle feature passe par la création d'une branche. Elle est ensuite fusionnée par merge requesst.

Chaque merge request nécessite d'être liée à au moins une Issue.

Le repo nécessite la fonction git-lfs pour supporter les formats de fichier unity.

## CI/CD

- push

Cette étape va compiler les images docker, et les pousser sur le registre hébergé par GitLab. Le registre est accessible seulement aux utilisateurs ayant les permissions sur le registre.

- deploy

Cette étape va stop les conteneurs en cours d'exécution, puis va mettre à jour les images à partir du registre GitLab, avant de relancer les conteneurs.


