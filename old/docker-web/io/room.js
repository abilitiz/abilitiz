const models = require('./models')
const helpers = require('./helpers');

class Room {
    /* the socket there is the room owner's socket */
    constructor(ownerSocket, roomName, mapParams, io) {
        this.io = io;
        this.roomName = roomName;
        this.master = ownerSocket;

        this.players = {};
        this.foods = [];
        this.bushes = [];
        this.hits = [];
        this.anims = [];

        this.init(mapParams);
    }

    init(parameters) {
        this.map = parameters.dim;
        this.foodMax = parameters.food.max;
        this.foodValInter = parameters.food.val;
        Object.keys(parameters.bushes).map((key) => {
            let bushesData = parameters.bushes[key];

            for (let b of bushesData.elements) {
                b.img = bushesData.img;
                this.addBush(b);
            }

        })
    }

    update() {

        this.updateCollision();
        this.updateCoolDown();
        // this.updateBush();
        // this.updateHits();
        this.updateFoods();
        this.updatePlayers();

        /* define distinct players vision */
        helpers.player.display(this);
        for (let p in this.players) {
            this.io.sockets.connected[p].emit('display update', this.players[p].display);
        }

        // DOCS
        // // sending to all clients in "game" room except sender
        // socket.to("game").emit("nice game", "let's play a game");
        //
        // // sending to all clients in "game1" and/or in "game2" room, except sender
        // socket.to("game1").to("game2").emit("nice game", "let's play a game (too)");
        //
        // // sending to all clients in "game" room, including sender
        // io.in("game").emit("big-announcement", "the game will start soon");
    }

    updateCollision() {
        /*en vrai faudrais faire un quadtree ,parce que le server il va jamais tenir le pauvre dude */
        for (const [indP, player] of Object.entries(this.players)) {

            for (let hit of this.hits) {
                if (player === hit.player) continue;
                let colliding = helpers.geometrics.circleCircleCollides(player, hit, true);
                if (colliding) {
                    hit.hasCollide = true;
                    /* collision actions // switch ? */
                    player.lives -= 2;

                    /* TODO créer une fonction pour désactiver tt les fonctionalitées user ( genre envoie/reception des input keyboard )*/
                    // if (players[indP].lives < 0) delete players[socket.id];
                }
            }

            for (let food of this.foods) {
                let colliding = helpers.geometrics.circleCircleCollides(player, food, true);
                if (colliding) {
                    player.eatFood(food);

                }
            }
        }
    }

    updateCoolDown() {
        for (const [ind, player] of Object.entries(this.players)) {
            if (player.attackCoolDown > 0) {
                player.attackCoolDown -= 60;
                this.players[ind] = player;
            }
        }
    }

    updateBush() {
        for (const b of this.bushes) {
            /* reset/initialize idPLayerArray who hide in the bush last frame*/
            b.hiders = [];

            for (let p in this.players) {

                if (helpers.geometrics.rectCircleCollides(b, this.players[p], true)) {
                    b.hiders.push(p);
                }
            }

            if (b.move) {
                b.moves();
            }

        }
    }

    updateHits() {
        let updatedHits = [];
        this.anims = [];

        for (let ele of this.hits) {
            if (ele.hasCollide) {
                this.anims.push({x: ele.x, y: ele.y, text: '-2'})
                continue;
            }
            ele.leftRange -= ele.speed;
            //s'il re   ste de la range on upd la pos & on ajoute au tableau sortant
            if (ele.leftRange > 0) {
                switch (ele.direction) {
                    case "left":
                        ele.x -= ele.speed
                        break;
                    case "left-up":
                        ele.x -= ele.speed
                        ele.y -= ele.speed
                        break;
                    case "left-down":
                        ele.x -= ele.speed
                        ele.y += ele.speed
                        break;
                    case "up":
                        ele.y -= ele.speed
                        break;
                    case "right":
                        ele.x += ele.speed
                        break;
                    case "right-up":
                        ele.x += ele.speed
                        ele.y -= ele.speed
                        break;
                    case "right-down":
                        ele.x += ele.speed
                        ele.y += ele.speed
                        break;
                    case "down":
                        ele.y += ele.speed
                        break;
                    default :
                        console.log('nane ?')
                }

                updatedHits.push(ele);
            }
        }
        this.hits = updatedHits;
    }

    updateFoods() {
        if (this.foodMax > this.foods.length) this.addFood();

        this.foods = this.foods.filter(food => {
            return food.hasCollide === false
        });
    }

    updatePlayers() {
        for (const [indP, player] of Object.entries(this.players)) {
            /*360-x as x = orbit speed */
            player.thenMove();

            player.orbit.angle > 360 ? player.orbit.angle -= (360 + 0.4) : player.orbit.angle -= 0.4;

            for (const orbital of player.orbit.orbitals) {
                orbital.updPos();
            }
        }
    }

    addPlayer(socketId) {
        this.players[socketId] = new models.Player(this, 0, 0);
    }

    addFood() {
        let position = helpers.geometrics.findRandomPosInMap(this.map);
        let val = helpers.math.randomIntFromInterval(this.foodValInter.min, this.foodValInter.max);
        let newFood = new models.Food(position.x, position.y, val);

        this.foods.push(newFood);
    }

    addBush(b) {
        let newB = new models.Bush(this, b.x, b.y, b.w, b.h, b.img, b.move);
        this.bushes.push(newB);
    }

}

module.exports = Room;