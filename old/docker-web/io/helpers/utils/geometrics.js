let math = require('./mathematics');

function rectCircleCollides(rect, circle, collide_inside) {
    // compute a center-to-center vector
    let half = {x: rect.w / 2, y: rect.h / 2};
    let center = {
        x: circle.x - (rect.x + half.x),
        y: circle.y - (rect.y + half.y)
    };
    // check circle position inside the rectangle quadrant
    let side = {
        x: Math.abs(center.x) - half.x,
        y: Math.abs(center.y) - half.y
    };
    if (side.x > circle.r || side.y > circle.r) // outside
        return false;
    if (side.x < -circle.r && side.y < -circle.r) // inside
        return collide_inside;
    if (side.x < 0 || side.y < 0) // intersects side or corner
        return true;
    // circle is near the corner
    return side.x * side.x + side.y * side.y < circle.r * circle.r;
}

function rectRectCollides(r1, r2) {
    return !(
        ((r1.y + r1.h) < (r2.y)) ||
        (r1.y > (r2.y + r2.h)) ||
        ((r1.x + r1.w) < r2.x) ||
        (r1.x > (r2.x + r2.w))
    );
}

function circleCircleCollides(c1, c2) {

    const
        dx = c1.x - c2.x,
        dy = c1.y - c2.y,
        dist = Math.sqrt(dx * dx + dy * dy);

    return dist < c1.r + c2.r;
}

function findRandomPosInMap(map) {
    let
        x = math.randomIntFromInterval(map.x , map.width),
        y = math.randomIntFromInterval(map.y, map.height);

    return {
        x: x,
        y: y
    }
}

function getStraightLineFormula(pointA, pointB, get_y_from_x = true) {
    /*
    * this function describe a mathematical function of the AB line in a X,Y plan
    * by default it describe f(x) = y or f( point.x ) = point.y
    * If you need to get the point.x by passing the point.y set arg.get_y_from_x to FALSE
    * */

    if (pointA.x - pointB.x === 0) throw "formula is the Y axis";
    let formula = undefined;
    /*k pour le coeff directeur de AB */
    let k = (pointB.y - pointA.y) / (pointB.x - pointA.x);
    // if( pointA.x > pointB.x ) k *= -1;

    /*z ordonné a l'origine ( en gros cest le décalage de la droite AB par rapport au point (0,0) */
    let z = pointA.y - (k * pointA.x);
    /*d pour la direction*/

    if (get_y_from_x) {
        formula = (x) => {
            return (k * x) + z;
        }
    } else {
        formula = (y) => {
            k = k !== 0 ? k : 1;
            return (y - z) / k;
        }
    }

    // console.log( formula(2) );
    return formula;
}

// define whether the C point is head or back related to AB line
function is_point_C_above_A_B_line(A, B, C) {
    /*
    * if point.x is above ( true ) or under the AB line (false)
    * if A.x < B.x (true) else (false)
    *
    * ex : point. x == c.x && a,b,c -> (x,y)
    * ex : fct( a(0,0) ,b(4,0), c(2,2) => true ( C is above AB line )
    * -----------------------------------------
    * ex : fct( a(0,0) ,b(4,0), c(2,-2) => false ( c is under AB line )
    * -------------------------------^-----^^^^^
    * ex : fct( a(4,0) ,b(0,0), c(2,-2) => true ( c is under AB line but AB line is negative )
    * ------------^-------^----------------^^^^-
    * ex : fct( a(4,0) ,b(0,0), c(2,2) => false ( c is above AB line but AB line is negative )
    * ------------------------------^------^^^^-   */
    if(  B.x - A.x === 0 ){
        return (A.y > B.y) === (C.x > A.x);
    }

    let AB_func = getStraightLineFormula(A, B);

    let C_on_AB = {x: C.x, y: AB_func(C.x)};

    if (A.x !== B.x) {
        let inverse = A.x <= B.x;

        let C_above_AB = (C_on_AB.y <= C.y);

        return C_above_AB === inverse;

    }

}

function degreeToRadiant(degreeAngle) {
    //
    // Exemple: conversion de 27 ° en radians :
    //     27 ° = 27°×π/180° = 0.471238
    return degreeAngle * (Math.PI / 180);
}


function getPointOnCircleByAngle(circle, degreeAngle) {
    let rad = degreeToRadiant(degreeAngle);

    return {
        x : (Math.cos(rad) * circle.r) + circle.x,
        y : (Math.sin(rad) * circle.r) + circle.y
    }

}


function getClosestOnCircularInterval(auditedVal, intBeg, intEnd, tails) {
    // TODO
    /* Ex : dans un interval circulair de 0 a 360 ( ou 0 = 360)
    // découpé 8 trouver de quel valeur du découpage de l'interval la donnée soumise est la plus proche*/

    //Pour un interval [intBeg, intEnd] shared in nb tails -> find closest tails of auditedVal
}


module.exports = {
    rectCircleCollides: rectCircleCollides,
    rectRectCollides: rectRectCollides,
    circleCircleCollides: circleCircleCollides,
    findRandomPosInMap: findRandomPosInMap,
    is_point_C_above_A_B_line: is_point_C_above_A_B_line,
    getPointOnCircleByAngle : getPointOnCircleByAngle
};