// check if an element exists in array using a comparer function
// comparer : function(currentElement)
Array.prototype.inArray = function (comparer) {
    for (let i = 0; i < this.length; i++) {
        if (comparer(this[i])) return true;
    }
    return false;
};

Array.prototype.pushIfNotExist = function (element, comparer) {
    if (!this.inArray(comparer)) {
        this.push(element);
    }
};


function groupBy(arr, property) {
    return arr.reduce(function (memo, x) {
        if (!memo[x[property]]) {
            memo[x[property]] = [];
        }
        memo[x[property]].push(x);
        return memo;
    }, {});
}

function removeCircular(obj) {
    const seen = new Map();
    const recurse = obj => {
        seen.set(obj, true);
        for (let [k, v] of Object.entries(obj)) {
            if (typeof v !== "object") continue;
            if (seen.has(v)) delete obj[k];
            else recurse(v);
        }
    }
    recurse(obj);
}

module.exports = {
    groupBy: groupBy,
    removeCircular: removeCircular
}