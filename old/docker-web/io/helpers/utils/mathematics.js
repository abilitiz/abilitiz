function randomIntFromInterval(min, max) { // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/* use with Ex: iA = {min : x, max : y } && refValInInterval in iA - Authored by A.Laidin */
function getRefValBetweenIntervals( iA, iB, refA ){
    let rangeA = (iA.max - iA.min ) , rangeB = (iB.min - iB.max );
    return iB.min - ( rangeB*( ( refA - iA.min )/rangeA ))
}


module.exports = {
    randomIntFromInterval : randomIntFromInterval,
    getRefValBetweenIntervals : getRefValBetweenIntervals
}