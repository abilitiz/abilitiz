const h_geometric = require("../utils/geometrics");
const h_array = require("../utils/array");
const models = require("../../models/index");


function display(room) {

    for (const [i, p] of Object.entries(room.players)) {
        //default display package - unformatted ( notparsable to JSON yet cause of circular references )
        p.display = {
            main : {},
            rectangle: [],
            circle: [],
            vision : {},
            map : p.room.map

                // {
                //     x : p.room.map.x,
                //         y : p.room.map.y,
                //     width : p.room.map.width,
                //     height :p.room.map.height,
                //     margin : p.room.map.margin
                // }
        }

        p.display.main = globalParser(p);

        //set the 'vison circle'
        // p.x p.y p.visionRaduis
        let visionCircle = {
            x: p.x,
            y: p.y,
            r: p.visionRaduis,
            a: p.visionAngle,
            c: "rgba(196, 241, 190,1)"
        }

        let linePointA = h_geometric.getPointOnCircleByAngle(visionCircle, p.visionAngle + 90),
            linePointB = h_geometric.getPointOnCircleByAngle(visionCircle, p.visionAngle - 90);


        //need refacto
        p.display.vision = visionCircle;

        //bush , orbitals, hit, player, food
        for (const b of room.bushes) {
            if (h_geometric.rectCircleCollides(b, visionCircle, true)) {
                p.display.rectangle.push(globalParser(b));
            }
        }

        for (let h of room.hits) {
            if (h_geometric.circleCircleCollides(h, visionCircle)) {
                p.display.circle.push(globalParser(h));
            }
        }

        for (let f of room.foods) {


            if (h_geometric.circleCircleCollides(f, visionCircle, false)
                && ! h_geometric.is_point_C_above_A_B_line(linePointA, linePointB, f)) {
                p.display.circle.push(globalParser(f));
            }

        }

        for (const [index, otherPLayer] of Object.entries(room.players)) {
            if( p === otherPLayer ) continue;

            if (h_geometric.circleCircleCollides(otherPLayer, visionCircle)
                && ! h_geometric.is_point_C_above_A_B_line(linePointA, linePointB, otherPLayer )) {
                p.display.circle.push(globalParser( otherPLayer ));
            }
        }
    }
}


// MOVE IN ROOM HELPERS
function globalParser(obj) {
    let copy = Object.assign({}, obj);


    switch (obj.constructor.name) {

        case    "Player":
            ['room', 'orbit', 'display'].forEach(e => delete copy[e]);
            break;

        case    "Bush":
            delete copy.room;
            break;

        case    "Hit":
            delete copy.player;
            break;

        case "Orbitals":
            delete copy.player;
            break;

        case    "Food":
            // console.log('im some food' , copy );
            break;


    }

    return copy;
}


//
//     //texture, ya des img, c'est chiant
//     let copiedBushes = [];
//     let splitBushesType = helpers.array.groupBy(this.bushes, 'img');
//
//     Object.keys(splitBushesType).map((type) => {
//         let elArr = [];
//         for (let bush of splitBushesType[type]) {
//             //copy
//             let cpBush = Object.assign({}, bush);
//             cpBush.img = undefined;
//             cpBush.room = undefined;
//             elArr.push(cpBush);
//         }
//
//         copiedBushes.push({img: type, elements: elArr});
//     })
//
//     //TODO modify hits to be accepted without ObjectValue - front modif aswell
//
// }

// return {
//     players: copiedPLayers,
//     orbitals: copiedOrbitals,
//     hits: Object.values(copiedHits),
//     bush: copiedBushes,
//     anims: this.anims,
//     foods: this.foods
// };

module.exports = {
    display: display
};