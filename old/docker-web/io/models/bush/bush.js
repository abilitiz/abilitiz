class Bush {
    constructor(room,x, y, w, h, img, move) {
        this.room = room;

        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.img = img;
        this.move = move;

        this.init();
    }
    init(){
        this.hiders =[];
        if( this.move ){
            if( this.move.axes.x ) {
                this.move.axes.x.initRange = this.move.axes.x.range;
                this.move.axes.x.dir = true
            }
            if( this.move.axes.y ) {
                this.move.axes.y.initRange = this.move.axes.y.range;
                this.move.axes.y.dir = true
            }
        }

    }

    /*la j'ai la flemme mais faut rename sa en move et rename la prop en moves ou autree ...*/
    moves(){
        let axes = this.move.axes;
        let xy = [ 'x' , 'y' ];

        for(const n of xy ){

            if( axes[n] ){
                if(  axes[n].range > axes[n].initRange  || axes[n].range < 0 ){
                    axes[n].dir = !axes[n].dir;
                }

                if( axes[n].dir ){
                    this[n] += this.move.speed;
                    axes[n].range -= this.move.speed;
                }else{
                    this[n] -= this.move.speed;
                    axes[n].range += this.move.speed;
                }
            }

        }
    }

}



module.exports = Bush;