class Orbitals {
    constructor(player, r) {
        this.player = player;
        this.r = r;
        this.color = 'rgba(0,0,255)';

        this.init();
    }

    init() {
        //depending on current this.player.orbitals ....
        this.id = this.player.orbit.voidSlots.shift();
        // 36 = 360 / 10<-(max orbitals on a player)
        this.updPos();
    }

    updPos(){

        let angle = ((this.id * 18) - this.player.orbit.angle);

        /* oui cest chiant a capter =>  la const est egale a PI / 180 */
        let radian = angle * 0.0174532925;

        this.x  = this.player.x  + (this.player.orbit.dist *  Math.cos(radian));
        this.y  =  this.player.y  + (this.player.orbit.dist *  Math.sin(radian));
    }
}

module.exports = Orbitals;