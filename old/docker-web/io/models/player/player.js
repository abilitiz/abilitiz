const Hit = require('../hit/hit.js');
const helpers = require("../../helpers");
const Orbitals = require('./orbitals');

class Player {
    constructor(room, x, y) {
        this.room = room;
        this.name = undefined;
        this.x = x;
        this.y = y;
        this.r = 15;

        this.dir = {'left': 0, 'right': 0, 'up': 0, 'down': 0};
        this.speed = 3;
        this.attackSpeed = 1.2;
        this.attackCoolDown = 0;
        this.color = "#" + ((1 << 24) * Math.random() | 0).toString(16);
        this.lives = 100;
        this.stacks = 60;
        this.visionRaduis = 350;
        this.visionAngle = 225;//right-down chronoClock, 0 at left
        this.display = {};


        this.orbit = {
            voidSlots: [...Array(20).keys()],
            orbitals: [],
            angle: 0,
            dist: 80
        }

    }

    move(direction) {

        if (direction.includes("move")) {
            this.dir[direction.replace("move ", "")] = 1;
        }

        if (direction.includes("stop")) {
            this.dir[direction.replace("stop ", "")] = 0;
        }
    }

    thenMove() {

        if (this.dir['left']) {
            this.x -= this.speed;
            if (this.x < 0) this.x += this.room.map.width;
        }

        if (this.dir['up']) {
            this.y -= this.speed;
            if (this.y < 0) this.y += this.room.map.height;
        }

        if (this.dir['right']) {
            this.x += this.speed;
            if (this.x > this.room.map.width) this.x -= this.room.map.width;
        }

        if (this.dir['down']) {
            this.y += this.speed;
            if (this.y > this.room.map.height) this.y -= this.room.map.height;
        }

    }

    shoot(direction) {
        if (this.attackCoolDown > 0) return;
        this.attackCoolDown += Math.round(1000 / this.attackSpeed);

        let newHit = new Hit(this, direction, 3, 6, 450);
        // this.room.hits.push(newHit);
    }

    watch( angle ){
        this.visionAngle = angle;
    }

    eatFood(food) {
        //do stuff like growing & stacking ...

        food.hasCollide = true;
        if (this.stacks > 270) return;
        let growRatio = helpers.math.getRefValBetweenIntervals(
            {min: 15, max: 50},
            {min: 0.005, max: 0.002},
            this.r);

        this.r *= 1 + food.value * growRatio;
        // this.h *= 1 + food.value * growRatio;
        this.stacks += food.value;

        console.log(this.stacks);
    }

    release(event = 'addOrbitasl') {
        //create balls dat make circle around player - like orbital stuff
        //doing it , the player rebase on his initial width
        //max 3 stack , releasing stacks gave player 1 - 3 - 6 orbitals
        console.log('player release his stack');


        let nbStack = this.useStacks();
        for (let i = 0; i < nbStack; i++) {
            this.addOrbital();
        }
    }

    useStacks() {
        let stacksValue = 0
        switch (true) {

            case this.stacks > 260 :
                stacksValue = 6;
                this.stacks -= 260
                break;

            case this.stacks > 140 :
                stacksValue = 3;
                this.stacks -= 140;
                break;

            case this.stacks > 60 :
                stacksValue = 1;
                this.stacks -= 60;
                break;
        }
        /* this step update player size depending on current nbStack */
        this.r = helpers.math.getRefValBetweenIntervals(
            {min: 0, max: 270},
            {min: 15, max: 50},
            this.stacks);

        return stacksValue;
    }

    addOrbital() {
        this.orbit.voidSlots.sort((a, b) => a - b);
        let o = new Orbitals(this, 4);
        this.orbit.orbitals.push(o);

    }

    removeOrbital(orbital) {
        //create a destroy() in orbital and climb in player >> add orbitals nb to p.orbit.voidSlots
    }

}

module.exports = Player;