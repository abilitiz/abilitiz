class Hit {
    constructor(player, direction, r, speed, range, color = 'rgba(231,12,12)') {
        this.player = player;
        this.r = r;
        this.speed = speed;
        this.range = range;
        this.direction = direction;
        this.color = color;

        this.init();
    }

    init() {
        this.x = this.player.x;
        this.y = this.player.y;
        this.leftRange = this.range;
        this.hasCollide = false;
    }
}

module.exports = Hit;