let Player = require('./player/player'),
    Food = require('./food/food'),
    Hit = require('./hit/hit'),
    Bush = require('./bush/bush')

module.exports = {
    Player: Player,
    Food: Food,
    Hit: Hit,
    Bush: Bush
}