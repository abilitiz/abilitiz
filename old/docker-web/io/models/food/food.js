class Food{
    constructor(x,y,value , color = 'rgb(153,50,204)') {
        this.x = x;
        this.y = y;
        this.value = value;
        this.color = color;

        this.init();
    }

    init(){
        this.r = this.value * 0.8;
        this.hasCollide = false;
    }

}

module.exports = Food;