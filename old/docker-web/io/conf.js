//TODO split event reception by keypress & keyrelease
const socketio = require('socket.io');
const fs = require('fs');

const helpers = require('./helpers/index');
const Room = require('./room');

module.exports = function (server) {
    // io server
    const io = socketio(server);
    const Rooms = {};
    const mapParams = JSON.parse(fs.readFileSync('io/parameters/maps/map1.json'));

    function updateAllRooms() {
        for (let room in Rooms) {
            Rooms[room].update();
        }
    }

    setInterval(updateAllRooms, 1000 / 50);


    io.on('connection', function (socket) {
        // new user connected
        socket.roomName = undefined;
        let player = undefined;

        //creation of new userSocket
        socket
            // room event decide in which room the user gonna join
            .on('room', function (room) {

                //if user didnt provide room name -> he owned one
                if (room === null) {
                    room = helpers.security.makeId(6);
                    Rooms[room] = new Room(socket.id, room, mapParams, io);
                }

                //if user provide an existing rooName -> his socket join the room and he become a player of it
                if (Rooms[room] !== undefined) {
                    socket
                        .join(room)
                        .emit('room', room);

                    socket.roomName = room;

                    Rooms[socket.roomName].addPlayer(socket.id);

                    player = Rooms[socket.roomName].players[socket.id];

                } else {
                    socket.emit('unavailable room');
                }


            })
            .on('username', function (username) {
                Rooms[socket.roomName].players[socket.id].name = username;
                /*TODO mettre a jour la recupération du json / room */
                socket.emit('init map', mapParams, true);
            })
            .on('disconnect', function () {
                if (Rooms[socket.roomName] !== undefined) {
                    delete Rooms[socket.roomName].players[socket.id];
                    //if room is void, delete

                    if (Object.getOwnPropertyNames(Rooms[socket.roomName].players).length === 0) {
                        delete Rooms[socket.roomName]
                    }
                    console.log("room restant => ", Object.getOwnPropertyNames(Rooms).length);
                }
            });


        // player actions

        //moves 'move left','stop left',etc
        ((action, direction) => {
            action.forEach(a => {
                direction.forEach(d => {
                    let ad = a + ' ' + d;
                    socket.on(ad, function () {
                        // console.log('called' , ad);
                        player.move(ad);
                    });
                })
            });
        })(['move', 'stop'], ['up', 'down', 'right', 'left']);


        socket
            .on('hit left', function () {
                player.shoot('left')
                player.watch( 0 );
            })
            .on('hit up', function () {
                player.shoot('up')
                player.watch( 90 );
            })
            .on('hit right', function () {
                player.shoot('right')
                player.watch( 180 );
            })
            .on('hit down', function () {
                player.shoot('down')
                player.watch( 270 );
            })
            .on('hit left-up', function () {
                player.shoot('left-up')
                player.watch( 45 );
            })
            .on('hit left-down', function () {
                player.shoot('left-down')
                player.watch( 315 );
            })
            .on('hit right-up', function () {
                player.shoot('right-up')
                player.watch( 135 );
            })
            .on('hit right-down', function () {
                player.shoot('right-down')
                player.watch( 225 );
            })
            .on('release', function () {
                player.release();
                // player.watch( 0 );
            })
        ;
    });
};
