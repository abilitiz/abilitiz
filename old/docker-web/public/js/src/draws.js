function onWindowResize() {
    localScreen.width = window.innerWidth;
    localScreen.height = window.innerHeight;
    canvas.setAttribute('width', localScreen.width + "px");
    canvas.setAttribute('height', localScreen.height + "px");
}

function drawMap(Context, Canvas, Display) {
    if (Display.map === undefined) return;

    Context.clearRect(0, 0, Canvas.width, Canvas.height);
    Context.beginPath();
    Context.rect(Display.map.x, Display.map.y, Display.map.width, Display.map.height);
    Context.fillStyle = "rgba(20, 20, 50,1)"; //"#C4F1BE";
    Context.fill();
    // Context.drawImage(heartImg, Display.map.width - 25, 5); // Or at whatever offset you like
    // Context.textAlign = "right";//c chiant ça reset a 'left' a chaque iteration
    // if (players[socket.id]) {
    //     Context.font = "12px Arial";
    //     Context.strokeText(players[socket.id].lives, Display.map.width - 28, 18);
    // }
    Context.closePath();

}

// TODO refacto split pieces in fct ...
function displayVision(Context, Display) {
    // console.log( display );
    let v = Display.vision;
    let p = Display.main;

    if (p === undefined || v === undefined) {
        console.log('game on but player not init');
        return;
    }

    /* DRAW PLAYER */
    Context.beginPath();
    Context.arc(p.x, p.y, p.r, 0, Math.PI * 2);
    // ctx.fillStyle = p.c;
    Context.fillStyle = "rgba(196, 241, 190,1)"; //"#
    Context.fill();
    Context.font = "10px Arial";
    Context.textAlign = "center";//c chiant ça reset a 'left' a chaque iteration
    // ctx.strokeText(name, x + r / 2, y - 4);
    Context.strokeText(p.name, p.x, p.y + 5);
    Context.closePath();

    /* DRAW VISION CONE */
    let intPI = parseAngleVisionToPI_interval(v.a); /* a -> angle (initially hold by entity player )*/

    Context.beginPath();
    Context.arc(v.x, v.y, v.r, intPI[0], intPI[1] /*+ (Math.PI * 3) /2*/);

    Context.fillStyle = "rgba(196, 241, 190,1)"; //"#
    Context.fill();
    Context.closePath();

    /* draw all circular form on the canvas */
    Display.circle.forEach(function ({x, y, r, color, name = false}) {

        Context.beginPath();
        /* canvas.arc( x,y,r, interval ( 0*PI(A) -> 2*PI(B) where A == B  )  */
        Context.arc(x, y, r, 0, 2 * Math.PI);
        Context.fillStyle = color;
        Context.fill();

        if (name) {
            Context.font = "10px Arial";
            Context.textAlign = "center";//c chiant ça reset a 'left' a chaque iteration
            // ctx.strokeText(name, x + r / 2, y - 4);
            Context.strokeText(name, x, y + 5);
        }
        Context.closePath();
    })

    /* DRAW BUSHES  -- TODO refacto => Display must draw ALL rectangle not only Bushes - need GENRERIC FCT */
    Display.rectangle.forEach(function ({x, y, img, hiders}) {

        Context.beginPath();
        if (hiders.includes(socket.id)) {
            Context.globalAlpha = 0.4;
            Context.drawImage(imgs.bushes[img], x, y);
            Context.globalAlpha = 1.0;
        } else {
            Context.drawImage(imgs.bushes[img], x, y);
        }
        Context.beginPath();
    });
}

/* return an uptodate version of Display(arg) */
function defineAnchor(Display, LocalScreen) {

    /*fill the player relative player position x , y */
    let canvasMainPlayerPosition = {x: 0, y: 0};

    if (Display.vision === undefined) return;

    // Display.vision.x == PLAYER CUR X
    switch (true) { // X

        case Display.map.width < LocalScreen.width:
            // console.log('X axes must be centered by map limit');
            canvasMainPlayerPosition.x = (LocalScreen.width / 2 - Display.map.width / 2) + Display.vision.x;
            break;

        // case no border displayed
        case !(Display.vision.x + Display.map.margin <= LocalScreen.width / 2) && !(Display.vision.x - Display.map.margin >= (Display.map.width - LocalScreen.width / 2)):
            // console.log('X axes must be centered by map limit');
            canvasMainPlayerPosition.x = LocalScreen.width / 2;
            break;

        //case left border is displayed
        case Display.vision.x + Display.map.margin <= LocalScreen.width / 2 :
            // console.log('X axes must be centered by map limit');
            canvasMainPlayerPosition.x = Display.map.margin + Display.vision.x;
            break;

        //case right border is displayed
        case Display.vision.x - Display.map.margin >= (Display.map.width - LocalScreen.width / 2):
            canvasMainPlayerPosition.x = LocalScreen.width - (Display.map.width - Display.vision.x) - Display.map.margin;
            break;

        default:
            console.log("default case", Display.vision.x + " - " + Display.map.margin + " >= " + (Display.map.width + " - " + LocalScreen.width / 2));
            break;
    }


    switch (true) { //Y

        case Display.map.height < LocalScreen.height:
            // console.log('Y axes must be centered by map limit');
            canvasMainPlayerPosition.y = (LocalScreen.height / 2 - Display.map.height / 2) + Display.vision.y;
            break;

        //case no border displayed
        case  !(Display.vision.y + Display.map.margin <= LocalScreen.height / 2) && !(Display.vision.y - Display.map.margin >= (Display.map.height - LocalScreen.height / 2)):
            canvasMainPlayerPosition.y = LocalScreen.height / 2;
            break;

        //case top border is displayed
        case  Display.vision.y + Display.map.margin <= LocalScreen.height / 2:
            canvasMainPlayerPosition.y = Display.map.margin + Display.vision.y;
            break;

        //case bottom border is displayed
        case Display.vision.y - Display.map.margin >= (Display.map.height - LocalScreen.height / 2):
            // console.log("case bottom border is displayed");
            canvasMainPlayerPosition.y = LocalScreen.height - (Display.map.height - Display.vision.y) - Display.map.margin;
            break;

        default:
            console.log("default case");
            break;
    }


    return canvasMainPlayerPosition;
}

/* assume Display(arg) is up to date tough defineAncor(fct)*/
function parseDisplayToRelative(Display) {

    let x_axe_shift = Display.main.x - Display.main.xServer,
        y_axe_shift = Display.main.y - Display.main.yServer;

    Display.circle.forEach((c, i) => {
        c.x += x_axe_shift;
        c.y += y_axe_shift;
    });

    Display.rectangle.forEach((c, i) => {
        c.x += x_axe_shift;
        c.y += y_axe_shift;
    });

    Display.vision.x += x_axe_shift;
    Display.vision.y += y_axe_shift;

    Display.map.x += x_axe_shift;
    Display.map.y += y_axe_shift;

}

// - - - -  -- - - - - - - - - - - -  - - - - - -  -- - - -  -- -  - - - - --
// function testy() {
//
//     ctx.globalAlpha = 0.5;
//     ctx.drawImage(bush, 200, 300);
//     ctx.globalAlpha = 1.0;
//
//
//     // ctx.beginPath();
//     // // put stroke color to transparent
//     // ctx.strokeStyle = "transparent"
//     // // draw rectablge towards right hand side
//     // ctx.rect(200,200,100,100);
//     // ctx.rect(300,300,100,100);
//     //
//     //
//     //
//     // // create linear gradient
//     // var grdLinear = ctx.createLinearGradient(200, 200, 400, 400);
//     // // Important bit here is to use rgba()
//     // grdLinear.addColorStop(0, "rgba(0, 0, 0, 0.2)");
//     // grdLinear.addColorStop(0.25, "rgba(0, 0, 0, 0.4)");
//     // grdLinear.addColorStop(0.5, "rgba(0, 0, 0, 0.6)");
//     // grdLinear.addColorStop(0.75, "rgba(0, 0, 0, 0.8)");
//     // grdLinear.addColorStop(1, 'rgba(0, 0, 0, 1)');
//     // // add gradient to rectangle
//     // ctx.fillStyle = grdLinear;
//     // // step below are pretty much standard to finish drawing an object to canvas
//     // ctx.fill();
//     // ctx.stroke();
//     // ctx.closePath();
// }