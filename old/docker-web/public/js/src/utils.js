function parseAngleVisionToPI_interval(angle) {
    let unity = Math.PI / 180;

    let targetLine = angle * unity;

    return [targetLine + (0.5 * Math.PI), targetLine - (0.5 * Math.PI)];
}
