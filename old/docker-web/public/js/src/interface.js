$(document).ready(function () {

    $('#canvas').hide();

    $('#roomChoiceModal').modal('show', {backdrop: 'static', keyboard: false});

    $('#createRoomBtn').click(function () {
        showUserNameModal();
    })

    $('#joinRoomBtn').click(function () {
        $('.createOrJoinTitle ').hide();
        $('.createOrJoinDiv ').hide();

        $('.joinRoomTitle').show();
        $('.joinRoomInputDiv').show();
        $('#roomValidationBtn')
            .parent().show()
            .click(function () {
                let roomName = $('.joinRoomInputDiv input').val();
                showUserNameModal(roomName);
            })
    })

    function showUserNameModal(roomName = undefined) {
        $('#roomChoiceModal').modal('hide');
        $('#userNameInput').modal('show', {backdrop: 'static', keyboard: false})

        $('#userNameInputBtn').click(function () {
            let userRes = $('#userNameInput input').val();
            if (userRes !== undefined && userRes !== '') {

                socket
                    .on('room', function (roomName) {
                        console.log('your room is ', roomName);
                        $('#roomNameTitle').text("Room : '" + roomName + "'");
                    })
                    .on('unavailable room', function () {
                        alert("Requested room doesn't exist :s");
                        document.location.reload();
                    })
                    .emit('room', roomName)
                    /* ERROR if roomEvent  arrived before usernameEvent, may dat be read async ? TOCHECK */
                    .emit('username', userRes.toUpperCase())


                $('#userNameInput').modal('hide');
                $('#canvas').show();

            } else {
                alert('pas de nom, pas de chocolat !')
            }
        })
    }

})