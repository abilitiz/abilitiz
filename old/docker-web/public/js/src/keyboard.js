function loadKeyboardEvents(Socket) {

    window.onkeydown = function (e) {
        keyboard[e.keyCode] = true;
        keyDownEvents(e.keyCode, Socket);

    };
    window.onkeyup = function (e) {
        delete keyboard[e.keyCode];
        keyUpEvents(e.keyCode, Socket);
    };
}

function keyDownEvents(e, Socket) {
    if (e === 37) Socket.emit('move left');
    if (e === 38) Socket.emit('move up');
    if (e === 39) Socket.emit('move right');
    if (e === 40) Socket.emit('move down');
}

function keyUpEvents(e, Socket) {
    if (e === 37) Socket.emit('stop left');
    if (e === 38) Socket.emit('stop up');
    if (e === 39) Socket.emit('stop right');
    if (e === 40) Socket.emit('stop down');
}

function playerInputs(Socket) {

    if (keyboard[32]) Socket.emit('release');

    switch (true) {
        case keyboard[81] && keyboard[90]:
            Socket.emit('hit left-up');
            break;
        case keyboard[81] && keyboard[83]:
            Socket.emit('hit left-down');
            break;
        case keyboard[68] && keyboard[90]:
            Socket.emit('hit right-up');
            break;
        case keyboard[68] && keyboard[83]:
            Socket.emit('hit right-down');
            break;
        case keyboard[81]:
            Socket.emit('hit left');
            break;
        case keyboard[90]:
            Socket.emit('hit up');
            break;
        case keyboard[68]:
            Socket.emit('hit right');
            break;
        case keyboard[83]:
            Socket.emit('hit down');
            break;
    }
}