/* server socket connection */
const socket = io();
/* case new user want join an existing room ( see interface.js) */
const urlParams = new URLSearchParams(window.location.search);
const roomName = urlParams.has('roomName') ? urlParams.get('roomName') : "";

/* canvas init*/
const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');

//TODO ...
const imgs = {bushes: []};
/* contain keys(num) which are CURRENTLY DOWN */
const keyboard = {};

/* explicit */
let localScreen = {"width": 0, "height": 0};
/* contain all displayed elements */
let display = {};
