let lastNow, deltaTime = 0;
window.onresize = onWindowResize;

function update(now) {
    /* frame interval in ms */
    deltaTime = Math.round((now - lastNow) * 100) / 100;
    lastNow = now;

    /*  listen user input */
    playerInputs(socket);

    /* draws */
    drawMap(ctx, canvas, display);
    displayVision(ctx, display);

    /* iterate */
    requestAnimationFrame(update);
}

function start() {
    console.log('upd 19-6-21');
    onWindowResize();
    loadKeyboardEvents(socket);
    requestAnimationFrame(update);
}

socket
    .on('init map', function (mapData, init = false) {
        // console.log( mapData );
        Object.keys(mapData.bushes).map((key) => {
            let imgName = mapData.bushes[key].img;
            imgs.bushes[imgName] = new Image();
            imgs.bushes[imgName].src = "img/" + imgName;
        })

        if (init) start();

    })
    .on('display update', function (thisPlayerDisplay) {
        // console.log(thisPlayerDisplay);
        display = thisPlayerDisplay;

        /*define where ON THE SCREEN is draw the user(current player) */
        let anc = defineAnchor(display, localScreen);
        /* bad code ... TODO need refacto  */
        display.main.xServer = display.main.x;
        display.main.yServer = display.main.y;
        display.main.x = anc.x;
        display.main.y = anc.y;

        /* update server position to localCanvas position (before draw) */
        /* WARNING parse only ON EVENT , prevent recursive parsing ... */
        parseDisplayToRelative(display);
    })

