<!-- Ce texte est un commentaire, qui sert de guide pour la création
des merge requests du projet. Ce texte n'apparaitra pas la description. -->


<!-- Voir le [Guide de collaboration](https://gitlab.com/abilitiz/abilitiz/-/blob/master/CONTRIBUTING.md). :

Les messages de commit doivent commencer avec un préfixe :

 * BUG: fix for runtime crash or incorrect result
 * COMP: compiler error or warning fix
 * DOC: documentation change
 * ENH: new functionality
 * PERF: performance improvement
 * STYLE: no logic impact (indentation, comments)
 * WIP: Work In Progress not ready for merge
 * REFAC: Refactoring the code

Ajouter un message pour décrire le changement apporté.

Enfin, ajouter les Issues liées au changement, exemple :

Fix #420, #666, #999

[Syntaxe](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)

-->

## Quel est le but de cette MR ?

<!-- Remplacer cette section par la descirption -->

## Issues

<!-- Remplacer cette section par les Fix # -->

## Checklist MR
- [ ] Documentation mise à jour.
- [ ] Le message de commit est approprié.
- [ ] La MR n'est pas trop grande.
- [ ] Les Issues ont été ajoutées.
- [ ] Les Labels ont été ajoutés.


<!-- Ne pas toucher -->
/milestone %
