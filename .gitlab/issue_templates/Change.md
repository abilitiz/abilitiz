## Résumé
<!-- Outline the issue being faced, and why this needs to change !-->

## Domaine(s)
<!-- This might only be one part, but may involve multiple sections !-->

## Fonctionnement actuel
<!-- the current process, and any associated business rules !-->

## Fonctionnement idéal
<!-- after the change, what should the process be, and what should the business rules be !-->
