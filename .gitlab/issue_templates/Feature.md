## Problème
<!-- What is the issue being faced and needs addressing? !-->

## Qui est impacté ?
<!-- Will this fix a problem that only one user has, or will it benefit a lot of people !-->

## Bénéfices et risques
<!--
    What benefits does this bring?
        - reduced support issues
        - save error prone manual checks
        - automate labour intensive tasks

    What risks might this introduce?
        - May result in more data being shared with staff
        - requires training materials to be updated
        - Involves working with a specific vendor for a fixed period.
!-->

## Solution proposée
<!-- How would you like to see this issue resolved? !-->

## Exemples
<!-- Are there any examples of this which exist in other software? !-->
